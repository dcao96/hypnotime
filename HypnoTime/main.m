//
//  main.m
//  HypnoTime
//
//  Created by David Cao on 6/13/13.
//  Copyright (c) 2013 David Cao. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HypnoAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HypnoAppDelegate class]));
    }
}
