//
//  HypnoAppDelegate.h
//  HypnoTime
//
//  Created by David Cao on 6/13/13.
//  Copyright (c) 2013 David Cao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HypnoAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
