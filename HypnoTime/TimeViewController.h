//
//  TimeViewController.h
//  HypnoTime
//
//  Created by David Cao on 6/13/13.
//  Copyright (c) 2013 David Cao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeViewController : UIViewController
{
    IBOutlet UILabel *timeLabel;
}
- (IBAction)showCurrentTime:(id)sender;
//- (void)viewDidAppear:(BOOL)animated;
- (void)viewWillAppear:(BOOL)animated;
//- (void)viewDidDisappear:(BOOL)animated;
- (void)viewWillDisappear:(BOOL)animated;

@end
