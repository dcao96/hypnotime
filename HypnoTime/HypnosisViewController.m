//
//  HypnosisViewController.m
//  HypnoTime
//
//  Created by David Cao on 6/13/13.
//  Copyright (c) 2013 David Cao. All rights reserved.
//

#import "HypnosisViewController.h"
#import "HypnosisterView.h"

@implementation HypnosisViewController
@synthesize sc;
@synthesize hv;

- (void)loadView
{
    CGRect frame = [[UIScreen mainScreen] bounds];
    HypnosisterView *v = [[HypnosisterView alloc] initWithFrame:frame];
    NSArray *colors = [NSArray arrayWithObjects:@"Blue", @"Red", @"Green", nil];
    sc = [[UISegmentedControl alloc] initWithItems:colors];
    //now you give it a frame
    CGRect scFrame = CGRectMake(60, 350, 200, 50);
    [sc setFrame:scFrame];
    [sc addTarget:self action:@selector(segmentedValueChanged:) forControlEvents:UIControlEventValueChanged];
    [v addSubview:sc];
    [self setView:v];
}

- (void)segmentedValueChanged:(id)sender
{
    switch ([sc selectedSegmentIndex]) {
        case 0:
            [[self view] setBackgroundColor:[UIColor blueColor]];
            break;
        case 1:
            [[self view] setBackgroundColor:[UIColor redColor]];
            break;
        case 2:
            [[self view] setBackgroundColor:[UIColor greenColor]];
            break;
        default:
            break;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"HypnosisViewController has loaded its view");
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nil bundle:nil];
    
    if(self)
    {
        UITabBarItem *t = [self tabBarItem];
        [t setTitle:@"Hypnosis"];
        UIImage *i = [UIImage imageNamed:@"Hypno.png"];
        [t setImage:i];
    
    }
    
    return self;
}

@end
