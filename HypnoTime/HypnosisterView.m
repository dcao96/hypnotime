//
//  HypnosisterView.m
//  Hypnosister
//
//  Created by Jianming Cao on 9/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HypnosisterView.h"

@implementation HypnosisterView

@synthesize circleColor;

- (void)setCircleColor:(UIColor *)clr
{
    circleColor = clr;
    [self setNeedsDisplay];
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [self setCircleColor:[UIColor lightGrayColor]];
    }
    return self;
}

- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if(motion == UIEventSubtypeMotionShake) {
        NSLog(@"Device is shaking!");
        CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
        CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
        CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
        UIColor *shakeColor = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
        
        [self setCircleColor:shakeColor];
    }
}


- (void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGRect bounds = [self bounds];
    
    //find center point
    CGPoint center;
    center.x = bounds.origin.x + bounds.size.width/2;
    center.y = bounds.origin.y + bounds.size.height/2;
    
    //Radius should be nearly as big as the view
    float maxRadius = hypot(bounds.size.width, bounds.size.height)/2;
    
    //sets thickness
    CGContextSetLineWidth(ctx, 10);
    
    //sets color
    [[self circleColor] setStroke];
    
    //Draws concentric circles from outside in
    for(float currentRadius = maxRadius; currentRadius > 0; currentRadius -=20) {
        CGContextAddArc(ctx, center.x, center.y, currentRadius,
                        0.0, M_PI * 2, YES);
        
        CGContextStrokePath(ctx);
    }
    
    //create a string
    NSString *text = @"You are getting sleepy";
    
    //Get a font
    UIFont *font = [UIFont boldSystemFontOfSize:28];
    
    CGRect textRect;
    
    textRect.size = [text sizeWithFont:font];
    
    //put text in center of view
    textRect.origin.x = center.x - textRect.size.width/2;
    textRect.origin.y = center.y - textRect.size.height/2;
    
    //set fill color of context to black
    [[UIColor blackColor] setFill];
    
    //shadow 4 points to the right and 3 down
    CGSize offset = CGSizeMake(4, 3);
    
    CGColorRef color = [[UIColor darkGrayColor] CGColor];
    
    //set shadow of context with these parameters
    //all drawings from now will be shadowed
    CGContextSetShadowWithColor(ctx, offset, 2.0, color);
    
    [text drawInRect:textRect
            withFont:font];
}

@end
