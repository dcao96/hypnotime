//
//  HypnosisViewController.h
//  HypnoTime
//
//  Created by David Cao on 6/13/13.
//  Copyright (c) 2013 David Cao. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HypnosisterView;

@interface HypnosisViewController : UIViewController
@property (strong, nonatomic) UISegmentedControl *sc;
@property (strong, nonatomic) HypnosisterView *hv;

- (void)segmentedValueChanged:(id)sender;

@end
