//#import "MapViewControllerSpec.h"
#import "MapViewController.h"

using namespace Cedar::Matchers;
using namespace Cedar::Doubles;

SPEC_BEGIN(MapViewControllerSpecSpec)

describe(@"MapViewControllerSpec", ^{
    __block MapViewController *mvc;

    
    beforeEach(^{
        mvc = [[MapViewController alloc] init];
    });
    
    it(@"should be a subclass of UIViewController", ^{
        mvc should be_instance_of([UIViewController class]).or_any_subclass();
    });
    
    describe(@"as a valid instance", ^{
        beforeEach(^{
            //[mvc pass a message for setup]
            [mvc loadView];
        });
        
        it(@"should have its view loaded", ^{
            [mvc isViewLoaded] should be_truthy;
        });
        
    });
    
    it(@"should have Map as its tab text", ^{
        UITabBarItem *tbi = [mvc tabBarItem];
        expect([tbi title]).to(equal(@"Map"));
    });

    
    
});

SPEC_END
