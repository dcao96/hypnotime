//
//  MapViewController.h
//  HypnoTime
//
//  Created by David Cao on 6/14/13.
//  Copyright (c) 2013 David Cao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController
{
    CLLocationManager *locationManager;
    IBOutlet MKMapView *worldView;
}


@end
