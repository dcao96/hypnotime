//
//  MapViewController.m
//  HypnoTime
//
//  Created by David Cao on 6/14/13.
//  Copyright (c) 2013 David Cao. All rights reserved.
//

#import "MapViewController.h"

@implementation MapViewController

- (void)viewDidLoad
{
    //sets the view to current location when opened
    
    //set map type to be satellite
    [worldView setMapType:1];
    [worldView setShowsUserLocation:YES];
    [super viewDidLoad];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self)
    {
        UITabBarItem *t = [self tabBarItem];
        [t setTitle:@"Map"];
        /*
        locationManager = [[CLLocationManager alloc] init];
        
        //error here...
        [locationManager setDelegate:self];
         
        [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
         */
    }
    return self;
}

- (void)dealloc
{
    [locationManager setDelegate:nil];
}

@end
